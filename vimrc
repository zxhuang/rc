"""""""""""""""""""
" syntax color
"""""""""""""""""""
syntax on
hi StatusLine       guifg=#CCCCCC     guibg=#202020     gui=italic    ctermfg=black    ctermbg=green    cterm=NONE
hi StatusLineNC     guifg=black       guibg=#202020     gui=NONE      ctermfg=darkgray ctermbg=white    cterm=NONE  
hi Comment          guifg=black       guibg=#202020     gui=NONE      ctermfg=green    ctermbg=darkgray cterm=NONE  


"""""""""""""""""""
" row & column
"""""""""""""""""""
set ruler
set nu
"set list
"set listchars=tab:>−,trail:−


"""""""""""""""""""
" tabs
"""""""""""""""""""
set expandtab
set tabstop=4
set shiftwidth=4
"set tabstop=2
"set shiftwidth=2
set nocompatible
set autoindent
set smartindent


"""""""""""""""""""
" search
"""""""""""""""""""
set showmatch
set incsearch
set hlsearch
set ignorecase
set smartcase


""""""""""""""""""""""""
" NerdTree
""""""""""""""""""""""""
nmap <silent> <c-n> :NERDTreeToggle<CR>


"""""""""""""""""""""
" taglist
"""""""""""""""""""""
let g:Tlist_Use_Right_Window = 1
nmap <silent> <c-l> :TlistToggle<CR>


""""""""""""""""""""""""""""
" auto complete
""""""""""""""""""""""""""""
"inoremap ( ()<LEFT>
"inoremap <C-f> <RIGHT>
"inoremap { {<CR>}<UP><END><CR>
